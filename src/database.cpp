//
// Created by karol on 2021-05-10.
//

#include "database.h"

extern "C" {
#include "pgc_database.h"
};

#include <cstring>
#include <cstdlib>

using namespace DBVS;

Database *Database::instance = nullptr;

Database *Database::getInstance() {
    if (Database::instance == nullptr)
        Database::instance = new Database();

    return Database::instance;
}

void Database::free() {
    delete Database::instance;
    Database::instance = nullptr;
}

Database::Database() = default;
Database::~Database() = default;

std::vector<Storage> Database::getAllStorages() const {
    int itemsReturned = 0;
    Storage *storages = SelectAllStorages(&itemsReturned);

    auto result = std::vector<Storage>();
    for (int i = 0; i < itemsReturned; i++) {
        result.push_back(storages[i]);
    }

    std::free(storages);

    return result;
}

std::vector<Storage> Database::getPageOfStorages(int page, int pageSize) const {
    int itemsReturned = 0;
    Storage *storages = SelectStoragesPaginated(page, pageSize, &itemsReturned);

    auto result = std::vector<Storage>();
    for (int i = 0; i < itemsReturned; i++) {
        result.push_back(storages[i]);
    }

    std::free(storages);

    return result;
}

std::vector<Part> Database::getPageOfParts(int page, int pageSize) const {
    int itemsReturned = 0;
    Part* parts = SelectPartsPaginated(page, pageSize, &itemsReturned);

    auto result = std::vector<Part>();
    for (int i = 0; i < itemsReturned; i++) {
        result.push_back(parts[i]);
    }

    std::free(parts);

    return result;
}

std::vector<Part> Database::getPartsByTitle(const std::string &title) const {
    int itemsReturned = 0;
    Part* parts = SelectPartsWithTitle(title.c_str(), &itemsReturned);

    auto result = std::vector<Part>();
    for (int i = 0; i < itemsReturned; i++) {
        result.push_back(parts[i]);
    }

    std::free(parts);

    return result;
}

std::vector<Car> Database::getAllCars() const {
    int itemsReturned = 0;
    Car* cars = SelectAllCars(&itemsReturned);

    auto result = std::vector<Car>();
    for (int i = 0; i < itemsReturned; i++) {
        result.push_back(cars[i]);
    }
    std::free(cars);

    return result;
}
