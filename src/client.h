//
// Created by karol on 2021-04-18.
//

#ifndef INC_2LAB_CLIENT_H
#define INC_2LAB_CLIENT_H

#include "commands.h"
#include "database.h"

#include <vector>

namespace DBVS {
    class Client {
    private:
        bool shouldRun = true;
        Database* database;
        L5RP::CommandManager* commandManager;

        void registerClientCommands();
        void greetUser();

        void printStorages(std::vector<Storage> const& storages);
        void printParts(std::vector<Part> const& parts);
        void printCars(std::vector<Car> const& cars);

    public:
        Client(Database* database);
        ~Client();

        void run();
        void stop();
    };
}


#endif //INC_2LAB_CLIENT_H
