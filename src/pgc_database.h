#ifndef PGC_DATABASE_H
#define PGC_DATABASE_H

typedef struct SStorage {
    int id;
    char title[129];
    char city[65];
    char street[65];
    char house[11];
    int size;
} Storage;

typedef struct SPart {
    int id;
    char title[129];
    char text[1024];
    char code[129];
    float cost;
    char vin[18];
    int storage_id;
} Part;

typedef struct SCar {
    char vin[18];
    char brand[65];
    char model[65];
    char date[11];
    float cost;
} Car;

#define RESULT_INT_SUCCESS 0
#define RESULT_INT_FAILURE 1

Storage* SelectAllStorages(int* itemsReturned);
Storage* SelectStoragesPaginated(int page, int pageSize, int* itemsReturned);
int insertStorage(Storage* storage);
int updateStorageTitle(int ID, const char* title);
int deleteStorage(int ID);

Part* SelectPartsPaginated(int page, int pageSize, int* itemsReturned);
Part* SelectPartsWithTitle(const char* title, int* itemsReturned);
int insertPartWithImages(Part* part, const char** images, int numOfImages);

Car* SelectAllCars(int* itemsReturned);

#endif