//
// Created by karol on 2021-04-18.
//

#include "client.h"
#include "variadicTable.h"

#include <stdexcept>
#include <iostream>
#include <chrono>
#include <climits>
#include <thread>
#include <cstring>
#include <cmath>

using namespace DBVS;
using namespace std;

Client::Client(Database *database) {
    this->database = database;
    this->commandManager = new L5RP::CommandManager(this);
    this->registerClientCommands();

    this->greetUser();
}

Client::~Client() = default;

void Client::registerClientCommands() {
    using namespace L5RP;

    new Command(
            this->commandManager,
            [this](std::vector<ArgumentValue> const &args) {
                int page = args[0]._union._int;
                int pageSize = 20;

                if (args.size() >= 2)
                    pageSize = args[1]._union._int;

                auto storages = this->database->getPageOfStorages(page, pageSize);
                this->printStorages(storages);
            },
            {
                    "storage", "storages"
            },
            {
                    new Argument(
                            new LimitedIntArgument(1, INT_MAX),
                            "Puslapis",
                            "int"
                    )
            },
            {
                    new Argument(
                            new LimitedIntArgument(1, INT_MAX),
                            "Puslapio dydis",
                            "int (default: 20)"
                    )
            },
            "Shows all storage areas"
    );

    new Command(
            this->commandManager,
            [this](std::vector<ArgumentValue> const &args) {
                int size;
                std::string title, city, street, house;

                std::cout << "Enter title: " << std::endl;
                std::getline(std::cin, title);

                std::cout << "Enter city: " << std::endl;
                std::getline(std::cin, city);

                std::cout << "Enter street: " << std::endl;
                std::getline(std::cin, street);

                std::cout << "Enter house: " << std::endl;
                std::getline(std::cin, house);

                do {
                    std::cout << "Enter size: " << std::endl;
                    std::cin >> size;

                    if(cin.fail())
                    {
                        cin.clear();
                        cin.ignore(10000, '\n');

                        std::cout << "Invalid number format!" << std::endl;
                        continue;
                    }

                    cin.ignore(10000, '\n');
                    break;
                } while(true);

                auto storage = Storage();
                strncpy(storage.title, title.c_str(), sizeof(storage.title));
                strncpy(storage.city, city.c_str(), sizeof(storage.city));
                strncpy(storage.street, street.c_str(), sizeof(storage.street));
                strncpy(storage.house, house.c_str(), sizeof(storage.house));
                storage.size = size;

                auto result = insertStorage(&storage);

                if(result == RESULT_INT_SUCCESS)
                    std::cout << "Storage was successfully created!" << std::endl;

                else
                    std::cout << "Storage was not created successfully!" << std::endl;
            },
            {
                    "create_storage",
            },
            {

            },
            {

            },
            "Creates a storage area"
    );

    new Command(
            this->commandManager,
            [this](std::vector<ArgumentValue> const &args) {
                int ID;
                std::string title;

                auto storages = this->database->getAllStorages();
                this->printStorages(storages);

                do {
                    std::cout << "Enter ID: " << std::endl;
                    std::cin >> ID;

                    if(cin.fail())
                    {
                        cin.clear();
                        cin.ignore(10000, '\n');

                        std::cout << "ID should be a whole number" << std::endl;
                        continue;
                    }

                    cin.ignore(10000, '\n');

                    bool found = false;
                    for (Storage const &storage : storages)
                    {
                        if(storage.id == ID)
                        {
                            found = true;
                            break;
                        }
                    }

                    if(!found) {
                        std::cout << "Invalid ID entered!" << std::endl;
                        continue;
                    }

                    break;
                } while (true);

                std::cout << "Enter title: " << std::endl;
                std::getline(std::cin, title);

                auto result = updateStorageTitle(ID, title.c_str());
                if(result == RESULT_INT_SUCCESS)
                    std::cout << "Storage was successfully edited!" << std::endl;

                else
                    std::cout << "Storage was not edited successfully!" << std::endl;
            },
            {
                    "edit_storage_title",
            },
            {

            },
            {

            },
            "Edits storage's area title"
    );

    new Command(
            this->commandManager,
            [this](std::vector<ArgumentValue> const &args) {
                int ID;

                auto storages = this->database->getAllStorages();
                this->printStorages(storages);

                do {
                    std::cout << "Enter ID: " << std::endl;
                    std::cin >> ID;

                    if(cin.fail())
                    {
                        cin.clear();
                        cin.ignore(10000, '\n');

                        std::cout << "ID should be a whole number" << std::endl;
                        continue;
                    }

                    cin.ignore(10000, '\n');

                    bool found = false;
                    for (Storage const &storage : storages)
                    {
                        if(storage.id == ID)
                        {
                            found = true;
                            break;
                        }
                    }

                    if(!found) {
                        std::cout << "Invalid ID entered!" << std::endl;
                        continue;
                    }

                    break;
                } while (true);

                auto result = deleteStorage(ID);
                if(result == RESULT_INT_SUCCESS)
                    std::cout << "Storage was successfully deleted!" << std::endl;

                else
                    std::cout << "Storage was not deleted successfully!" << std::endl;
            },
            {
                    "delete_storage",
            },
            {

            },
            {

            },
            "Deletes storage area"
    );

    new Command(
            this->commandManager,
            [this](std::vector<ArgumentValue> const &args) {
                int page = args[0]._union._int;
                int pageSize = 20;

                if (args.size() >= 2)
                    pageSize = args[1]._union._int;

                auto parts = this->database->getPageOfParts(page, pageSize);
                this->printParts(parts);
            },
            {
                    "parts"
            },
            {
                    new Argument(
                            new LimitedIntArgument(1, INT_MAX),
                            "Puslapis",
                            "int"
                    )
            },
            {
                    new Argument(
                            new LimitedIntArgument(1, INT_MAX),
                            "Puslapio dydis",
                            "int (default: 20)"
                    )
            },
            "Shows all parts"
    );

    new Command(
            this->commandManager,
            [this](std::vector<ArgumentValue> const &args) {
                int storage_id, numOfImages;
                float cost;
                std::string title, text, code, vin;
                std::vector<std::string> images;

                std::cout << "Enter title: " << std::endl;
                std::getline(std::cin, title);

                std::cout << "Enter description: " << std::endl;
                std::getline(std::cin, text);

                std::cout << "Enter code: " << std::endl;
                std::getline(std::cin, code);

                do {
                    std::cout << "Enter price: " << std::endl;
                    std::cin >> cost;

                    if(cin.fail())
                    {
                        cin.clear();
                        cin.ignore(10000, '\n');

                        std::cout << "Invalid number format!" << std::endl;
                        continue;
                    }

                    cin.ignore(10000, '\n');
                    break;
                } while(true);
                cost = std::floor(cost * 100) / 100;

                auto cars = this->database->getAllCars();
                this->printCars(cars);

                do {
                    std::cout << "Enter vin: " << std::endl;
                    std::getline(std::cin, vin);

                    if(vin.length() > 17) {
                        std::cout << "VIN may only contain 17 characters" << std::endl;
                        continue;
                    }

                    break;
                } while(true);

                auto storages = this->database->getAllStorages();
                this->printStorages(storages);

                do {
                    std::cout << "Enter storage ID: " << std::endl;
                    std::cin >> storage_id;

                    if(cin.fail())
                    {
                        cin.clear();
                        cin.ignore(10000, '\n');

                        std::cout << "Invalid number format!" << std::endl;
                        continue;
                    }

                    bool found = false;
                    for (Storage const &storage : storages)
                    {
                        if(storage.id == storage_id)
                        {
                            found = true;
                            break;
                        }
                    }

                    if(!found) {
                        std::cout << "Invalid ID entered!" << std::endl;
                        continue;
                    }

                    cin.ignore(10000, '\n');
                    break;
                } while(true);

                do {
                    std::cout << "Enter number of images: " << std::endl;
                    std::cin >> numOfImages;

                    if(cin.fail())
                    {
                        cin.clear();
                        cin.ignore(10000, '\n');

                        std::cout << "Invalid number format!" << std::endl;
                        continue;
                    }

                    cin.ignore(10000, '\n');
                    break;
                } while(true);

                for(int i = 0; i < numOfImages; i++)
                {
                    std::cout << "Enter " << i + 1 << " image URL: " << std::endl;

                    std::string url;
                    std::getline(std::cin, url);
                    images.push_back(url);
                }

                Part part = {};
                strncpy(part.title, title.c_str(), sizeof(part.title));
                strncpy(part.text, text.c_str(), sizeof(part.text));
                strncpy(part.code, code.c_str(), sizeof(part.code));
                part.cost = cost;
                strncpy(part.vin, vin.c_str(), sizeof(part.vin));
                part.storage_id = storage_id;

                std::vector<const char*> imagesURL;
                for(std::string const& image : images)
                    imagesURL.push_back(image.c_str());

                auto result = insertPartWithImages(&part, imagesURL.data(), imagesURL.size());

                if(result == RESULT_INT_SUCCESS)
                    std::cout << "Part was successfully created!" << std::endl;

                else
                    std::cout << "Part was not created successfully!" << std::endl;
            },
            {
                    "create_part",
            },
            {

            },
            {

            },
            "Creates a part"
    );

    new Command(
            this->commandManager,
            [this](std::vector<ArgumentValue> const &args) {
                if(args[0]._string.find('%') != std::string::npos)
                    throw "Search contents cannot contain %";

                auto parts = this->database->getPartsByTitle(args[0]._string);
                this->printParts(parts);
            },
            {
                    "search_part", "search_parts"
            },
            {
                    new Argument(
                            new StringDumpArgument(),
                            "title",
                            "title of part"
                    )
            },
            {},
            "Search part by title"
    );
}

void Client::greetUser() {
    std::cout << "Welcome to car inventory management system" << std::endl;
    std::cout << "Write help to see all available commands" << std::endl;
}

void Client::printStorages(const std::vector<Storage> &storages) {
    if(storages.empty())
        throw "No storages found!";

    VariadicTable<int, std::string, std::string, std::string, std::string, int> vt(
            {"ID", "Title", "City", "Street", "House Number", "Size"}
    );

    for (Storage const &storage : storages)
        vt.addRow(
                storage.id,
                std::string(storage.title),
                std::string(storage.city),
                std::string(storage.street),
                std::string(storage.house),
                storage.size
        );

    vt.print(std::cout);
}

void Client::printParts(const std::vector<Part> &parts) {
    if(parts.empty())
        throw "No parts found!";

    VariadicTable<int, std::string, std::string, std::string, float, std::string, int> vt(
            {"ID", "Title", "Description", "Code", "Cost", "VIN", "storage_id"}
    );

    for (Part const &part : parts)
        vt.addRow(
                part.id,
                std::string(part.title),
                std::string(part.text),
                std::string(part.code),
                part.cost,
                std::string(part.vin),
                part.storage_id
        );

    vt.print(std::cout);
}

void Client::printCars(const std::vector<Car> &cars) {
    if(cars.empty())
        throw "No cars found!";

    VariadicTable<std::string, std::string, std::string, std::string, float> vt(
            {"VIN", "Brand", "Model", "Made (date)", "cost"}
    );

    for (Car const &car : cars)
        vt.addRow(
                std::string(car.vin),
                std::string(car.brand),
                std::string(car.model),
                std::string(car.date),
                car.cost
        );

    vt.print(std::cout);
}

void Client::run() {
    while (this->shouldRun) {
        std::string input;
        std::getline(std::cin, input);

        if (!input.empty())
            this->commandManager->tryToRunCommand(input);

        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

}

void Client::stop() {
    this->shouldRun = false;
}