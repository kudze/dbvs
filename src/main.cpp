//
// Created by karol on 2021-05-10.
//

#include "client.h"

#include <iostream>

using namespace DBVS;

int main()
{
    try {
        auto client = new Client(Database::getInstance());
        client->run();

        Database::free();
    } catch(std::exception const& e) {
        std::cout << "ERROR" << std::endl << e.what() << std::endl;
    }

    return 0;
}