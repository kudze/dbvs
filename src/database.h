//
// Created by karol on 2021-05-10.
//

#ifndef INC_2LAB_DATABASE_H
#define INC_2LAB_DATABASE_H

#include <string>
#include <stdexcept>
#include <exception>
#include <vector>

extern "C" {
#include "pgc_database.h"
};

namespace DBVS {

    class Database {
        static Database* instance;

        Database();
        ~Database();
    public:
        std::vector<Storage> getAllStorages() const;
        std::vector<Storage> getPageOfStorages(int page, int pageSize = 20) const;
        std::vector<Part> getPageOfParts(int page, int pageSize = 20) const;
        std::vector<Part> getPartsByTitle(std::string const& title) const;
        std::vector<Car> getAllCars() const;

        static Database* getInstance();
        static void free();
    };

}


#endif //INC_2LAB_DATABASE_H
