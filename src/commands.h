//
// Created by karol on 2021-04-18.
//

#ifndef INC_2LAB_COMMANDS_H
#define INC_2LAB_COMMANDS_H

#include <cstdint>
#include <string>
#include <stdexcept>
#include <vector>
#include <sstream>
#include <functional>
#include <map>
#include <unordered_map>

namespace DBVS {
    class Client;
}

namespace L5RP {

    //Eech really bad soliution, but i cant think of anything better atm.
    union _ArgumentValue {
        bool _bool;
        int _int;
        uint32_t _uint32_t;
        float _float;
        uint8_t _uint8_t;
        void *_ptr;
    };

    struct ArgumentValue {
        _ArgumentValue _union;
        std::string _string;
    };

    class ArgumentParseException
            : public std::exception {
        std::string msg;

    public:

        explicit ArgumentParseException(std::string const &msg);

        const char * what() const noexcept override;

    };

    class ArgumentOutOfLimits
            : public ArgumentParseException {

    public:

        explicit ArgumentOutOfLimits(std::string const &msg);

    };

    class ChatException {
        std::vector<std::string> messages;

    public:

        explicit ChatException(
                std::vector<std::string> const& messages
        );

        std::vector<std::string> const& getMessages() const;

    };

    class IArgument {

    public:

        enum InputType {
            NONE,
            WORD,
            DUMP
        };

    private:

        InputType inputType;

    public:

        explicit IArgument(
                InputType inputType
        );
        virtual ~IArgument();

        /**
         * @throws ArgumentParseException
         */
        virtual ArgumentValue parseValue(std::string const &str) = 0;

        InputType getInputType() const;

    };

    class IntArgument
            : public IArgument {

    public:

        IntArgument();
        virtual ~IntArgument() = default;

        /**
         * @throws ArgumentParseException
         */
        virtual ArgumentValue parseValue(std::string const &str);

        static int parse(std::string const &str);
    };

    class UnsignedInt32Argument
            : public IArgument {

    public:

        UnsignedInt32Argument();
        virtual ~UnsignedInt32Argument() = default;

        /**
         * @throws ArgumentParseException
         */
        virtual ArgumentValue parseValue(std::string const &str);

        static uint32_t parse(std::string const &str);
    };

    /**
     * Basically IntArgument but has min max values.
     */
    class LimitedIntArgument
            : public IArgument {

        int min;
        int max;

    public:

        /**
         * min - Smallest allowed variable.
         * max - Biggest allowed variable.
         */
        LimitedIntArgument(int min, int max);
        virtual ~LimitedIntArgument() = default;


        virtual ArgumentValue parseValue(std::string const &str);

        static int parse(std::string const &str, int min, int max);

        void setMin(int min);
        void setMax(int max);

        int getMin() const;
        int getMax() const;
    };

    /**
     * Same as LimitedIntArgument, but instead of throwing exception
     * returns specified default value
     */
    class LimitedDefaultIntArgument
            : public IArgument {

        int min;
        int max;
        int def;

    public:

        LimitedDefaultIntArgument(int min, int max, int def);
        virtual ~LimitedDefaultIntArgument() = default;


        virtual ArgumentValue parseValue(std::string const &str);
        static int parse(std::string const &str, int min, int max, int def);
        int getDefault() const;
    };

    /**
     * Basically is a uint8_t argument.
     */
    class ByteArgument
            : public IArgument {

    public:

        ByteArgument();
        virtual ~ByteArgument() = default;


        /**
         * @throws ArgumentParseException
         */
        virtual ArgumentValue parseValue(std::string const &str);

        static uint8_t parse(std::string const &str);

    };

    class FloatArgument
            : public IArgument {

    public:

        FloatArgument();
        virtual ~FloatArgument() = default;

        /**
         * @throws ArgumentParseException
         */
        virtual ArgumentValue parseValue(std::string const &str);

        static float parse(std::string const &str);

    };

    /**
     * Basically FloatArgument but has min max values.
     */
    class LimitedFloatArgument
            : public IArgument {
        float min;
        float max;

    public:

        /**
         * min - Smallest allowed variable.
         * max - Biggest allowed variable.
         */
        LimitedFloatArgument(float min, float max);
        virtual ~LimitedFloatArgument() = default;

        virtual ArgumentValue parseValue(std::string const &str);

        static float parse(std::string const &str, float min, float max);

        float getMin() const;

        float getMax() const;

    };

    /**
     * Same as LimitedFloatArgument, but instead of throwing exception
     * returns specified default value
     */
    class LimitedDefaultFloatArgument
            : public IArgument {

        float min;
        float max;
        float def;

    public:

        LimitedDefaultFloatArgument(float min, float max, float _defaut);
        virtual ~LimitedDefaultFloatArgument() = default;


        virtual ArgumentValue parseValue(std::string const &str);

        static float parse(std::string const &str, float min, float max, float def);

        float getDefault() const;

    };

    class StringArgument
            : public IArgument {

    public:

        StringArgument();
        virtual ~StringArgument() = default;

        /**
         * @throws ArgumentParseException
         */
        virtual ArgumentValue parseValue(std::string const &str);

    };

    /**
     * This will allow strings with spaces inside of it.
     * Only should be last mandatory or optional argument.
     * However, there's no validation for that.
     */
    class StringDumpArgument
            : public IArgument {

    public:

        StringDumpArgument();
        virtual ~StringDumpArgument() = default;

        /**
         * @throws ArgumentParseException
         */
        virtual ArgumentValue parseValue(std::string const &str);

    };

    /**
     * Either true, or false.
     */
    class BoolArgument
            : public IArgument {

    public:

        BoolArgument();
        virtual ~BoolArgument() = default;


        /**
         * @throws ArgumentParseException
         */
        virtual ArgumentValue parseValue(std::string const &str);

        static bool parse(std::string const &str);

    };

    class CallableArgumentCountException
            : public ChatException {

    public:

        explicit CallableArgumentCountException(
                std::initializer_list<std::string> const &exception
        );

        explicit CallableArgumentCountException(
                std::vector<std::string> const &exception
        );

    };

    template <typename T>
    class Callable {

        std::vector<IArgument *> mandatoryArgs;
        std::vector<IArgument *> optionalArgs;

    protected:

        /**
         * You can override this if you want to generate custom exceptions.
         */
        virtual void throwArgsCountException() {
            throw CallableArgumentCountException(
                    {
                            "Wrong number of arguments was passed to callable!"
                    }
            );
        }

    public:

        explicit Callable(
                std::vector<IArgument *> const &mandatoryArgs,
                std::vector<IArgument *> const &optionalArgs
        ) {
            this->mandatoryArgs = mandatoryArgs;
            this->optionalArgs = optionalArgs;
        }

        virtual ~Callable() {
            for (IArgument *arg : this->mandatoryArgs)
                delete arg;
            this->mandatoryArgs.clear();

            for (IArgument *arg : this->optionalArgs)
                delete arg;
            this->optionalArgs.clear();
        }

        /**
         * Only throws CallableArgumentCountException,
         * if mandatory arguments are not provided.
         *
         * @throws CallableArgumentCountException
         * @throws ArgumentParseException
         */
        virtual T onExecute(std::string const &str) {
            std::vector<std::string> res;

            std::stringstream args(str);

            std::string arg;
            while (args >> arg)
                res.push_back(arg);

            return this->onExecute(res);
        }

        virtual T onExecute(std::vector<std::string> const &args) {
            //Lets count how many words we want to execute.
            size_t len = 0;
            for (auto argument : this->mandatoryArgs)
                if (argument->getInputType() != IArgument::InputType::NONE)
                    len++;

            if (args.size() < len)
                this->throwArgsCountException();

            else {

                std::vector<ArgumentValue> result;

                size_t currArg = 0;
                for (size_t i = 0; i < args.size(); i++) {

                    IArgument *currArgument = nullptr;

                    while (true) {

                        if (i < this->mandatoryArgs.size())
                            currArgument = this->mandatoryArgs.at(i);

                        else if (i < this->mandatoryArgs.size() + this->optionalArgs.size())
                            currArgument = this->optionalArgs.at(i - this->mandatoryArgs.size());

                        else
                            break;

                        if (currArgument->getInputType() != IArgument::InputType::NONE)
                            break;

                        result.push_back(currArgument->parseValue(""));
                        currArg++;
                    }

                    if (currArgument == nullptr)
                        break;

                    //Some handling for StringDumpArgument.
                    if (currArgument->getInputType() == IArgument::DUMP) {

                        //If argument is of StringDumpArgument type.
                        //We want to construct string with all remaining args.

                        std::string constructedString = args[i];
                        for (size_t j = i + 1; j < args.size(); j++)
                            constructedString += " " + args[j];

                        result.push_back(currArgument->parseValue(constructedString));

                        break;
                    }

                    //Then we can parse our string and add it to result.
                    //NOTE: We dont want to handle any exceptions here.
                    result.push_back(currArgument->parseValue(args.at(i)));
                    currArg++;

                }

                return this->onExecute(result);
            }

            return T();
        }

        virtual T onExecute(std::vector<ArgumentValue> const &args) = 0;

        std::vector<IArgument *> const &getMandatoryArgs() const {
            return this->mandatoryArgs;
        }
        std::vector<IArgument *> const &getOptionalArgs() const {
            return this->optionalArgs;
        }
    };

    class Argument
    {

        IArgument* argument;
        std::string name;
        std::string desc;

    public:

        Argument(
                IArgument* argument,
                std::string const& name,
                std::string const& desc
        );

        IArgument* getArgument() const;
        std::string const& getName() const;
        std::string const& getDescription() const;
    };

    class CommandManager;
    class Command
            : public Callable<void>
    {

    private:

        CommandManager* manager;
        std::string desc;
        std::vector<std::string> names;
        std::vector<Argument*> mandatoryArgs;
        std::vector<Argument*> optionalArgs;
        std::function<void(std::vector<ArgumentValue> const&)> fn;

    protected:

        void throwArgsCountException() override;

    public:
        Command(
                CommandManager* manager,
                std::function<void(std::vector<ArgumentValue> const&)> const& fn,
                std::initializer_list<std::string> const& names,
                std::initializer_list<Argument*> const& mandatoryArgs,
                std::initializer_list<Argument*> const& optionalArgs,
                std::string const& description
        );
        virtual ~Command();

        void onExecute(std::string const& args) override;
        void onExecute(std::vector<std::string> const& args) override;
        void onExecute(std::vector<ArgumentValue> const& args) override;

        CommandManager* getCommandManager() const;
        std::string const& getDescription() const;
    };

    class CommandManager {
        friend Command;

        DBVS::Client* client;

        std::map<std::string, Command*> commands;
        void registerCommand(Command* command, std::string const& name);

    public:
        explicit CommandManager(DBVS::Client* client);
        ~CommandManager();

        void tryToRunCommand(std::string const& command);

        DBVS::Client* getClient() const;
    };
}

#endif //INC_2LAB_COMMANDS_H
