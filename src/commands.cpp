//
// Created by karol on 2021-04-18.
//

#include "commands.h"
#include "client.h"

L5RP::ArgumentParseException::ArgumentParseException(std::string const &msg)
        : std::exception() {
    this->msg = msg;
}

const char *L5RP::ArgumentParseException::what() const noexcept {
    return this->msg.c_str();
}

L5RP::ArgumentOutOfLimits::ArgumentOutOfLimits(std::string const &msg)
        : ArgumentParseException(msg) {}

L5RP::UnsignedInt32Argument::UnsignedInt32Argument()
        : IArgument(
        InputType::WORD
) {

}

L5RP::ArgumentValue L5RP::UnsignedInt32Argument::parseValue(std::string const &str) {

    ArgumentValue result = {};
    result._union = {};
    result._union._uint32_t = UnsignedInt32Argument::parse(str);
    return result;

}

uint32_t L5RP::UnsignedInt32Argument::parse(std::string const &str) {
    try {
        return static_cast<uint32_t>(std::stoul(str));
    }

    catch (...) {
        throw ArgumentParseException(str + " is not whole number!");
    }
}

L5RP::IntArgument::IntArgument()
        : IArgument(
        InputType::WORD
) {

}

L5RP::ArgumentValue L5RP::IntArgument::parseValue(std::string const &str) {
    ArgumentValue result = {};
    result._union = {};
    result._union._int = IntArgument::parse(str);
    return result;
}

int L5RP::IntArgument::parse(std::string const &str) {
    try {
        return std::stoi(str);
    }

    catch (...) {
        throw ArgumentParseException(str + " is not whole number!");
    }
}

L5RP::IArgument::IArgument(InputType inputType) {
    this->inputType = inputType;
}

L5RP::IArgument::~IArgument() = default;

L5RP::IArgument::InputType L5RP::IArgument::getInputType() const {
    return this->inputType;
}

L5RP::LimitedIntArgument::LimitedIntArgument(
        int min,
        int max
)
        : IArgument(
        InputType::WORD
) {

    this->min = min;
    this->max = max;

}

L5RP::ArgumentValue L5RP::LimitedIntArgument::parseValue(
        std::string const &str
) {

    ArgumentValue value = {};
    value._union._int = LimitedIntArgument::parse(
            str,
            this->min,
            this->max
    );
    return value;

}

int L5RP::LimitedIntArgument::parse(std::string const &str, int min, int max) {

    int val = IntArgument::parse(str);

    if (val < min || val > max)
        throw ArgumentOutOfLimits(
                str +
                " should not be smaller than " + std::to_string(min) +
                " and not bigger than " + std::to_string(max)
        );

    return val;

}

void L5RP::LimitedIntArgument::setMin(int min) {
    this->min = min;
}

void L5RP::LimitedIntArgument::setMax(int max) {
    this->max = max;
}

int L5RP::LimitedIntArgument::getMin() const {
    return this->min;
}

int L5RP::LimitedIntArgument::getMax() const {
    return this->max;
}

L5RP::LimitedDefaultIntArgument::LimitedDefaultIntArgument(int min, int max, int def)
        : IArgument(
        InputType::WORD
) {
    this->min = min;
    this->max = max;
    this->def = def;
}

L5RP::ArgumentValue L5RP::LimitedDefaultIntArgument::parseValue(std::string const &str) {

    ArgumentValue value = {};
    value._union._int = LimitedDefaultIntArgument::parse(
            str,
            this->min,
            this->max,
            this->def
    );
    return value;

}

int L5RP::LimitedDefaultIntArgument::parse(std::string const &str, int min, int max, int _default) {

    try {

        return LimitedIntArgument::parse(str, min, max);

    } catch (ArgumentOutOfLimits const &) {

        return _default;

    }

}

int L5RP::LimitedDefaultIntArgument::getDefault() const {
    return this->def;
}

L5RP::ArgumentValue L5RP::ByteArgument::parseValue(std::string const &str) {
    ArgumentValue result;

    result._union._uint8_t = ByteArgument::parse(str);

    return result;
}

L5RP::ByteArgument::ByteArgument()
        : IArgument(
        InputType::WORD
) {

}

uint8_t L5RP::ByteArgument::parse(std::string const &str) {

    return static_cast<uint8_t>(
            LimitedIntArgument::parse(str, 0, 255)
    );

}

L5RP::FloatArgument::FloatArgument()
        : IArgument(
        InputType::WORD
) {

}

L5RP::ArgumentValue L5RP::FloatArgument::parseValue(std::string const &str) {

    ArgumentValue value = {};
    value._union._float = FloatArgument::parse(str);
    return value;

}

float L5RP::FloatArgument::parse(std::string const &str) {

    try {
        return std::stof(str);
    }

    catch (...) {
        throw ArgumentParseException(str + " is not a number!");
    }

}

L5RP::LimitedFloatArgument::LimitedFloatArgument(
        float min,
        float max
)
        : IArgument(
        InputType::WORD
) {

    this->min = min;
    this->max = max;

}

L5RP::ArgumentValue L5RP::LimitedFloatArgument::parseValue(
        std::string const &str
) {

    ArgumentValue value = {};
    value._union._float = LimitedFloatArgument::parse(
            str,
            this->min,
            this->max
    );
    return value;

}

std::string FLOAT_TO_STRING(float value, uint8_t precision = 2) {
    std::string full = std::to_string(value);

    return full.substr(0, full.find('.') + precision + 1);
}

float L5RP::LimitedFloatArgument::parse(std::string const &str, float min, float max) {

    float val = FloatArgument::parse(str);

    if (val < min || val > max)
        throw ArgumentOutOfLimits(
                str +
                " turėtų būti ne mažesnis, kaip " + FLOAT_TO_STRING(min) +
                " ir ne didesnis, kaip " + FLOAT_TO_STRING(max)
        );

    return val;

}

float L5RP::LimitedFloatArgument::getMin() const {
    return this->min;
}

float L5RP::LimitedFloatArgument::getMax() const {
    return this->max;
}

L5RP::LimitedDefaultFloatArgument::LimitedDefaultFloatArgument(
        float min,
        float max,
        float def
) : IArgument(
        InputType::WORD
) {
    this->min = min;
    this->max = max;
    this->def = def;
}

L5RP::ArgumentValue L5RP::LimitedDefaultFloatArgument::parseValue(std::string const &str) {
    ArgumentValue value = {};
    value._union._float = LimitedDefaultFloatArgument::parse(
            str,
            this->min,
            this->max,
            this->def
    );
    return value;
}

float L5RP::LimitedDefaultFloatArgument::parse(std::string const &str, float min, float max, float _default) {
    try {

        return LimitedFloatArgument::parse(
                str,
                min,
                max
        );

    } catch (ArgumentOutOfLimits const &) {

        return _default;

    }
}

float L5RP::LimitedDefaultFloatArgument::getDefault() const {
    return this->def;
}

L5RP::StringArgument::StringArgument()
        : IArgument(
        InputType::WORD
) {

}

L5RP::ArgumentValue L5RP::StringArgument::parseValue(std::string const &str) {
    ArgumentValue result = {};
    result._string = str;
    return result;
}

L5RP::StringDumpArgument::StringDumpArgument()
        : IArgument(
        InputType::DUMP
) {

}

L5RP::ArgumentValue L5RP::StringDumpArgument::parseValue(std::string const &str) {
    ArgumentValue result = {};
    result._string = str;
    return result;
}

L5RP::BoolArgument::BoolArgument()
        : IArgument(
        InputType::WORD
) {

}

L5RP::ArgumentValue L5RP::BoolArgument::parseValue(std::string const &str) {

    ArgumentValue result = {};
    result._union._bool = BoolArgument::parse(str);
    return result;

}

bool L5RP::BoolArgument::parse(std::string const &str) {

    if (str == "true" || str == "1")
        return true;

    if (str == "false" || str == "0")
        return false;

    throw ArgumentParseException(str + " can only be true or false!");

}

L5RP::CallableArgumentCountException::CallableArgumentCountException(
        std::initializer_list<std::string> const &msg
)
        : ChatException(std::vector<std::string>(msg)) {

}

L5RP::CallableArgumentCountException::CallableArgumentCountException(
        std::vector<std::string> const &msg
)
        : ChatException(msg) {

}

L5RP::ChatException::ChatException(
        std::vector<std::string> const &messages
) {
    this->messages = messages;
}

std::vector<std::string> const &L5RP::ChatException::getMessages() const {
    return this->messages;
}

L5RP::Argument::Argument(IArgument *argument, std::string const &name, std::string const &desc) {
    this->argument = argument;
    this->name = name;
    this->desc = desc;
}

std::string const &L5RP::Argument::getName() const {
    return this->name;
}

std::string const &L5RP::Argument::getDescription() const {
    return this->desc;
}

L5RP::IArgument *L5RP::Argument::getArgument() const {
    return this->argument;
}

std::vector<L5RP::IArgument *> ARGUMENT_CONVERTER(std::initializer_list<L5RP::Argument *> args) {

    std::vector<L5RP::IArgument *> result;

    for (
        L5RP::Argument *arg : args
            )
        result.push_back(arg->getArgument());

    return result;

}

void L5RP::Command::throwArgsCountException() {

    std::vector<std::string> msg;
    std::string msg1 = u8"* Usage: " + this->names.at(0);

    for(Argument* arg : this->mandatoryArgs)
        msg1 += u8" <" + arg->getName() + u8">";

    for(Argument* arg : this->optionalArgs)
        msg1 += u8" [" + arg->getName() + u8"]";

    msg.push_back(msg1);

    for(Argument* arg : this->mandatoryArgs)
        msg.push_back(arg->getName() + u8" - " + arg->getDescription());

    for(Argument* arg : this->optionalArgs)
        msg.push_back(arg->getName() + u8" - " + arg->getDescription());

    if(this->names.size() != 1) {

        std::string msg2 = u8"* This command also has aliases: ";

        for(size_t i = 1; i < this->names.size(); i++)
            msg2 += this->names[i] + u8" ";

        msg.push_back(msg2);

    }

    throw CallableArgumentCountException(msg);


}

L5RP::Command::Command(
        CommandManager *console,
        std::function<void(std::vector<ArgumentValue> const &)> const &fn,
        std::initializer_list<std::string> const &names,
        std::initializer_list<Argument *> const &mandatoryArgs,
        std::initializer_list<Argument *> const &optionalArgs,
        std::string const &desc
)
        : Callable<void>(
        ARGUMENT_CONVERTER(mandatoryArgs),
        ARGUMENT_CONVERTER(optionalArgs)
) {

    this->manager = console;
    this->fn = fn;
    this->desc = desc;
    this->names = std::vector<std::string>(names);
    this->mandatoryArgs = std::vector<Argument *>(mandatoryArgs);
    this->optionalArgs = std::vector<Argument *>(optionalArgs);

    for (std::string const &name : names)
        this->getCommandManager()->registerCommand(this, name);

}

L5RP::Command::~Command() {
    for (Argument *arg : this->mandatoryArgs)
        delete arg;

    for (Argument *arg : this->optionalArgs)
        delete arg;
}

void L5RP::Command::onExecute(std::string const &args) {
    L5RP::Callable<void>::onExecute(args);
}

void L5RP::Command::onExecute(std::vector<std::string> const &args) {
    L5RP::Callable<void>::onExecute(args);
}

void L5RP::Command::onExecute(std::vector<ArgumentValue> const &args) {
    this->fn(args);
}

std::string const &L5RP::Command::getDescription() const {
    return this->desc;
}

L5RP::CommandManager *L5RP::Command::getCommandManager() const {
    return this->manager;
};

#include <iostream>
#include <cmath>

#define HELP_CMD_COUNT_PER_PAGE 10

L5RP::CommandManager::CommandManager(DBVS::Client *client) {
    this->client = client;

    new Command(
            this,
            [](std::vector<ArgumentValue> const &args) {
                for (uint32_t i = 0; i < 20; i++)
                    std::cout << std::endl;
            },
            {
                    "clear"
            },
            {},
            {},
            "Clears window"
    );

    new Command(
            this,
            [this](std::vector<ArgumentValue> const &args) {
                this->getClient()->stop();
            },
            {
                    "exit"
            },
            {},
            {},
            "Stops the client"
    );

    new Command(
            this,
            [this](std::vector<ArgumentValue> const &args) {
                //Lets fetch all commands.
                std::map<std::string, Command *> const &allCommands = this->commands;

                //Lets count page count.
                int pageCount = (int) std::ceil(allCommands.size() / HELP_CMD_COUNT_PER_PAGE) + 1;

                //Lets determine which page we want to render.
                int page = 1;
                if (!args.empty())
                    page = args[0]._union._int;

                //Some validation.
                if (page < 1 || page > pageCount)
                    std::cout << "This page doesnt exist" << std::endl;

                else {

                    //Some more counting.
                    int begin = (page - 1) * HELP_CMD_COUNT_PER_PAGE;
                    int end = page * HELP_CMD_COUNT_PER_PAGE;

                    //Rendering
                    std::cout << "Help menu | Page: " << page << u8"/" << pageCount << std::endl;

                    auto it = allCommands.begin();

                    if (begin != 0)
                        std::advance(it, begin);

                    for (int i = begin; i < allCommands.size() && i < end; i++) {
                        std::cout << u8"* " << it->first << u8" - " << it->second->getDescription() << u8"!"
                                  << std::endl;

                        if (it != allCommands.end())
                            it++;

                    }

                }
            },
            {
                    "help"
            },
            {},
            {
                    new Argument(
                            new IntArgument(),
                            u8"page",
                            u8"Shows nth page of commands"
                    )
            },
            "Shows this help menu"
    );

}

#include <algorithm>

L5RP::CommandManager::~CommandManager() {
    std::vector<Command *> deleted;

    for (std::pair<std::string, Command *> pair : this->commands) {
        if (std::find(deleted.begin(), deleted.end(), pair.second) == deleted.end()) {
            delete pair.second;
            deleted.push_back(pair.second);
        }
    }

}

using namespace std;

void L5RP::CommandManager::tryToRunCommand(std::string const &command) {
    std::string name;
    std::string args;
    auto it = command.find(' ');
    if (it == string::npos)
        name = string(command);

    else {
        name = command.substr(0, it);
        args = command.substr(it + 1);
    }

    try {
        this->commands.at(name)->onExecute(args);
    }

    catch (ChatException const &e) {
        for (auto const &_msg : e.getMessages())
            std::cout << _msg << std::endl;
    }

    catch (std::out_of_range const &) {
        std::cout << "* This command doesnt exist!" << std::endl;
    }

    catch (std::exception const &e) {
        std::cout << "ERROR: " << e.what() << std::endl;
    }

    catch (std::string const& e) {
        std::cout << "ERROR: " << e << std::endl;
    }

    catch (const char* e) {
        std::cout << "ERROR: " << e << std::endl;
    }
}

DBVS::Client *L5RP::CommandManager::getClient() const {
    return this->client;
}

void L5RP::CommandManager::registerCommand(Command *command, const std::string &name) {
    this->commands[name] = command;
}
